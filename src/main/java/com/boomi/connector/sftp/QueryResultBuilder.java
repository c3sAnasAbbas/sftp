//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp;

import java.io.IOException;
import java.io.InputStream;

import com.boomi.connector.api.PayloadUtil;
import com.boomi.connector.sftp.actions.RetryableQueryAction;
import com.boomi.connector.sftp.results.BaseResult;
import com.jcraft.jsch.ChannelSftp.LsEntry;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
public class QueryResultBuilder implements ResultBuilder {

	private InputStream fileContent;

	public InputStream getFilecontent() {
		return fileContent;
	}

	@Override
	public BaseResult makeResult(LsEntry meta, String dirFullPsth, RetryableQueryAction retryableGetaction)
			throws IOException {
		try {
			retryableGetaction.execute();
			fileContent = (retryableGetaction).getOutputStream().toInputStream();
			return new BaseResult(PayloadUtil.toPayload(fileContent));

		}
		
		finally {
			(retryableGetaction).close();
		}

	}

}
