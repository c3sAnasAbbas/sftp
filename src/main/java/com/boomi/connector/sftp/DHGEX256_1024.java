//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp;

import com.jcraft.jsch.Session;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class DHGEX256_1024 extends DHGEX1024 {
	public void init(Session session, byte[] V_S, byte[] V_C, byte[] I_S, byte[] I_C) throws Exception {
		this.hash = "sha-256";
		super.init(session, V_S, V_C, I_S, I_C);
	}
}
