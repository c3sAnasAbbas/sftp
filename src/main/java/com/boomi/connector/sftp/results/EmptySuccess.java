//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.results;

import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.TrackedData;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class EmptySuccess
implements Result {
    @Override
    public void addToResponse(OperationResponse response, TrackedData input) {
        ResponseUtil.addEmptySuccess((OperationResponse)response, (TrackedData)input, (String)"0");
    }
}

