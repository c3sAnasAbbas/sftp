//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.exception;

 /**
  * @author Omesh Deoli
  *
  * ${tags}
  */
public class NoSuchFileFoundException extends RuntimeException {

	private static final long serialVersionUID = -6908750692873284245L;

	public NoSuchFileFoundException(String statusMessage) {
		super(statusMessage);
		
	}

	public NoSuchFileFoundException(String errMessage, Throwable e) {
		super(errMessage, e);
	}

}
