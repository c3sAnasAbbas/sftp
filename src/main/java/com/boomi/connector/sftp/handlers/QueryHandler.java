//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.handlers;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.FilterData;
import com.boomi.connector.api.JsonPayloadUtil;
import com.boomi.connector.api.Payload;
import com.boomi.connector.sftp.CustomLsEntrySelector;
import com.boomi.connector.sftp.FileQueryFilter;
import com.boomi.connector.sftp.ResultBuilder;
import com.boomi.connector.sftp.SFTPClient;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.actions.RetryableLsEntrySelector;
import com.boomi.connector.sftp.common.FileMetadata;
import com.boomi.connector.sftp.common.PathsHandler;
import com.boomi.connector.sftp.common.PropertiesUtil;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.connector.sftp.results.MultiResult;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;
import com.boomi.util.json.JSONUtil;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.ChannelSftp.LsEntrySelector;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
public class QueryHandler {

	SFTPConnection conn;

	final PathsHandler pathsHandler;

	public static Payload makeJsonPayload(LsEntry meta, String dirFullPath) {
		return QueryHandler.makeJsonPayload(meta, dirFullPath, true);
	}

	private static Payload makeJsonPayload(LsEntry meta, String dirFullPath, boolean includeAll) {
		ObjectNode obj = JSONUtil.newObjectNode();
		obj.put(SFTPConstants.PROPERTY_FILENAME, meta.getFilename());
		obj.put(SFTPConstants.PROPERTY_REMOTE_DIRECTORY, dirFullPath);
		if (includeAll) {
			obj.put(SFTPConstants.MODIFIED_DATE,
					FileMetadata.formatDate(FileMetadata.parseDate(meta.getAttrs().getMTime() * 1000l)));
			obj.put(SFTPConstants.FILESIZE, meta.getAttrs().getSize());
			obj.put(SFTPConstants.IS_DIRECTORY, meta.getAttrs().isDir());
		}
		return JsonPayloadUtil.toPayload((TreeNode) obj);
	}

	public QueryHandler(SFTPConnection conn) {

		this.conn = conn;
		pathsHandler = conn.getPathsHandler();

	}

	String toFullPath(String childPath) {
		return this.pathsHandler.resolvePaths(conn.getHomeDirectory(), childPath);
	}

	public void doQuery(MultiResult result, boolean filesOnly, ResultBuilder resultBuilder) {
		FilterData input = result.getInput();

		String enteredRemoteDir = conn.getEnteredRemoteDirectory(input);
		String remoteDir;
		if (StringUtil.isBlank(enteredRemoteDir) || !pathsHandler.isFullPath(enteredRemoteDir)) {
			 input.getLogger().log(Level.INFO,
						"Entered remote directory path is blank or not a absolute full path.Setting home directory of user as default working path");
			remoteDir = this.toFullPath(enteredRemoteDir);
		} else {
			remoteDir = enteredRemoteDir;
		}

		if (remoteDir == null) {
			throw new ConnectorException(SFTPConstants.ERROR_MISSING_DIRECTORY);
		}
		Logger logger = input.getLogger();
		File directory = new File(remoteDir);

		FileQueryFilter filter = conn.makeFilter(input, filesOnly, directory, remoteDir);
		SFTPClient newClient = null;
		LsEntrySelector selector = null;
		RetryableLsEntrySelector selectAction = null;
		try {

			long limit = conn.getLimit();
			newClient = conn.createSftpClient(new PropertiesUtil(conn.getContext()));
			selector = new CustomLsEntrySelector(result, filter, limit, logger, resultBuilder, remoteDir, conn,
					newClient);
			selectAction = new RetryableLsEntrySelector(conn, remoteDir, input, selector);
			selectAction.execute();

		} finally {
			IOUtil.closeQuietly(newClient);
		}
	}

}
