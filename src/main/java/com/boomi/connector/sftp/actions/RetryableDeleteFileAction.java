//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.actions;

import com.boomi.connector.api.ObjectIdData;
import com.boomi.connector.api.TrackedData;

import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.results.Result;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class RetryableDeleteFileAction extends SingleRetryAction {
	private ObjectIdData fileId;
	private Result result;

	public RetryableDeleteFileAction(SFTPConnection connection, String remoteDir,TrackedData input) {
		super(connection, remoteDir, input);

	}

	public RetryableDeleteFileAction(SFTPConnection connection, ObjectIdData input) {
		super(connection, null, input);
		this.fileId = input;
	}

	@Override
	public void doExecute() {
		result = this.getConnection().deleteFile(fileId);
	}

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}
}
