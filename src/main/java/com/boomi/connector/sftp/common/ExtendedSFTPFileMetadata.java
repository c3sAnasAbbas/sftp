//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.common;

import java.nio.file.Path;
import java.nio.file.Paths;

import com.boomi.connector.api.PayloadMetadata;
import com.boomi.connector.api.PayloadMetadataFactory;

import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class ExtendedSFTPFileMetadata extends SimpleSFTPFileMetadata {
	private final String timestamp;

	public ExtendedSFTPFileMetadata(String remoteDir, String fileName, String timeStamp) {
		super(remoteDir, fileName);
		this.timestamp = timeStamp;
	}

	@Override
	public PayloadMetadata toPayloadMetadata(PayloadMetadataFactory payloadMetadataFactory) {
		PayloadMetadata metadata = super.toPayloadMetadata(payloadMetadataFactory);
		metadata.setTrackedProperty("timestamp", this.timestamp);
		return metadata;
	}

	@Override
	protected ObjectNode toJson() {
		return super.toJson().put("timestamp", this.timestamp);
	}

	public Path getPath() {
		UnixPathsHandler pathHandler = new UnixPathsHandler();
		return Paths.get(pathHandler.joinPaths(super.getDirectory(), super.getName()));

	}
}
