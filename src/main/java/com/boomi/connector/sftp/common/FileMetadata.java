//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.common;

import com.boomi.connector.sftp.actions.RetryableGetFileMetadataAction;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.util.StringUtil;
import com.jcraft.jsch.SftpATTRS;

import java.io.File;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
public class FileMetadata {
	private final Path path;
	private final String fileName;
	private final String parent;
	private SftpATTRS attrs;

	private Date modifiedDate;
	private static final String FILE_PATH_SEPARATOR = "/";

	public FileMetadata(Path path, RetryableGetFileMetadataAction retryableFilemetadataction) {
		if (path == null) {
			throw new IllegalArgumentException(SFTPConstants.INVALID_PATH);
		}
		this.path = path;
		File file = path.toFile();
		this.fileName = file.getName();
		this.parent = file.getParent();
		retryableFilemetadataction.set_fileName(this.fileName);
		retryableFilemetadataction.execute();
		this.attrs = retryableFilemetadataction.get_fileMetaData();
		this.modifiedDate = getParsedModifiedDate();
	}

	private Date getParsedModifiedDate() {

		try {
			return parseDate(formatDate(parseDate(attrs.getMTime() * 1000l)));
		} catch (ParseException e) {
			throw new IllegalArgumentException(SFTPConstants.ERROR_PARSING_DATE_FROM_FILESYSTEM, e);
		}
	}

	public Path getPath() {
		return this.path;
	}

	public String getFileName() {
		return this.fileName;
	}

	public String getParent() {
		return this.parent;
	}

	private SftpATTRS getAttrs() {

		return this.attrs;
	}

	public Date getModifiedDate() {
		this.getAttrs();
		return this.modifiedDate;
	}

	public String getModifiedDateString() {
		return FileMetadata.formatDate(this.getModifiedDate());
	}

	public long getSize() {
		return this.getAttrs().getSize();
	}

	public boolean isRegularFile() {
		return (!this.getAttrs().isLink()) && (!this.getAttrs().isDir());
	}

	public boolean isDirectory() {
		return this.getAttrs().isDir();
	}

	public static String formatDate(Date date) {
		return FileMetadata.iso8601Format().format(date);
	}

	public static Date parseDate(String date) throws ParseException {
		return new FileDate(FileMetadata.iso8601Format().parse(date));
	}

	public static Date parseDate(long date) {
		return new FileDate(date);
	}

	private static SimpleDateFormat iso8601Format() {
		SimpleDateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		return utcFormat;
	}

	public static String joinPaths(String parentPath, String childRelativePath) {
		if (StringUtil.isBlank((String) parentPath)) {
			return childRelativePath;
		}
		if (StringUtil.isBlank((String) childRelativePath)) {
			return parentPath;
		}
		String result = parentPath + (!parentPath.endsWith(FILE_PATH_SEPARATOR) ? FILE_PATH_SEPARATOR : "")
				+ childRelativePath;
		return result.replaceAll("\\\\", "/");
	}

	private static class FileDate extends Date {
		private static final long serialVersionUID = 4045331070L;

		FileDate(long date) {
			super(date);
		}

		FileDate(Date date) {
			this(date.getTime());
		}

		@Override
		public String toString() {
			return FileMetadata.formatDate(this);
		}
	}

}
