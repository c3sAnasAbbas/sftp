//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.common;

import com.boomi.util.TempOutputStream;
import java.io.IOException;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class MeteredTempOutputStream
extends TempOutputStream {
    private long count;

    public long getCount() {
        return this.count;
    }

    public void write(int b) throws IOException {
        super.write(b);
        ++this.count;
    }

    public void write(byte[] b, int off, int len) throws IOException {
        super.write(b, off, len);
        this.count += (long)len;
    }
}

