//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.retry;

import com.boomi.util.retry.PhasedRetry;
import com.boomi.util.retry.RetryStrategy;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
final class LimitedRetryStrategyFactory extends RetryStrategyFactory {
	private final int maxRetries;

	LimitedRetryStrategyFactory(int maxRetries) {
		this.maxRetries = maxRetries;
	}

	@Override
	public RetryStrategy createRetryStrategy() {
		return new PhasedRetry(this.maxRetries);
	}
}
