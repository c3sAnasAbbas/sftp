//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp;

import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.sftp.common.MeteredTempOutputStream;
import com.boomi.connector.sftp.common.PropertiesUtil;
import com.boomi.connector.sftp.common.SSHOptions;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.connector.sftp.exception.NoSuchFileFoundException;
import com.boomi.connector.sftp.exception.SFTPSdkException;
import com.boomi.util.IOUtil;
import com.boomi.util.StringUtil;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.ChannelSftp.LsEntrySelector;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.jce.SignatureDSA;
import com.jcraft.jsch.jce.SignatureRSA;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
public class SFTPClient implements Closeable {

	private ChannelSftp sftpChannel;
	private JSch jsch;
	private Session session;
	private String host;
	private int port;
	private AuthType authType;
	private PublicKeyParam publicKeyparam;
	private PasswordParam passwordParam;
	private final SSHOptions sshOptions;
	private String retFileName = null;
	private boolean filePresence=false;

	private Logger logger = Logger.getLogger(SFTPClient.class.getName());

	public SFTPClient(String host, int port, AuthType authType, PublicKeyParam publicKeyparam,
			PropertiesUtil properties) {
		this.host = host;
		this.port = port;
		this.authType = authType;
		this.publicKeyparam = publicKeyparam;
		this.sshOptions = new SSHOptions(properties);

	}

	public SFTPClient(String host, int port, AuthType authType, PasswordParam passwordParam,
			PropertiesUtil properties) {
		this.host = host;
		this.port = port;
		this.authType = authType;
		this.passwordParam = passwordParam;
		this.sshOptions = new SSHOptions(properties);

	}

	public void openConnection() {
		if (session == null || !session.isConnected()) {
			getSession();
		}
		if (sftpChannel == null || !sftpChannel.isConnected()) {
			getChannel();
		}
	}

	public void getSession() {

		ByteArrayInputStream knownHosts = null;
		try {
			jsch = new JSch();

			Properties config = null;

			boolean usepk = false;
			boolean useKnownHosts = false;
			boolean useDHKeySize1024 = true;
			String pkpath = null;
			String pkpassword = null;
			if (sshOptions != null) {
				usepk = sshOptions.isSshkeyauth();
				pkpath = sshOptions.getSshkeypath();
				pkpassword = sshOptions.getSshkeypassword();
				useKnownHosts = !StringUtil.isBlank((String) sshOptions.getKnownHostEntry());
				if (useKnownHosts) {
					knownHosts = new ByteArrayInputStream(sshOptions.getKnownHostEntry().getBytes(SFTPConstants.UTF_8));
					jsch.setKnownHosts((InputStream) knownHosts);
				}
				useDHKeySize1024 = sshOptions.isDhKeySizeMax1024();
			}
			if (usepk && !StringUtil.isBlank(pkpath)) {
				try {
					jsch.addIdentity(pkpath, pkpassword);
				} catch (JSchException e) {
					throw new ConnectorException(SFTPConstants.UNABLE_TO_PARSE_SSH_KEY + e.getMessage(), (Throwable) e);
				}
			}
			if (this.host != null) {

				config = new Properties();
				config.setProperty(SFTPConstants.PROP_STRICT_HOST_KEY_CHECKING, SFTPConstants.SHKC_NO);
				config.setProperty(SFTPConstants.PREFERRED_AUTHENTICATIONS, SFTPConstants.AUTH_SEQUENCE_FULL);
				config.put(SFTPConstants.KEY_COMP_S2C_ALG, SFTPConstants.COMP_ALGS);
				config.put(SFTPConstants.KEY_COMP_C2S_ALG, SFTPConstants.COMP_ALGS);
				config.put(SFTPConstants.SIGNATURE_DSS_KEY, SignatureDSA.class.getCanonicalName());
				config.put(SFTPConstants.SIGNATURE_RSA_KEY, SignatureRSA.class.getCanonicalName());
				if (useDHKeySize1024) {
					config.put(SFTPConstants.DH_GROUP_EXCHANGE_SHA1, SFTPConstants.CLASS_DHGEX1024);
					config.put(SFTPConstants.DH_GROUP_EXCHANGE_SHA256, SFTPConstants.CLASS_DHGEX256_1024);
					config.put(SFTPConstants.KEX, SFTPConstants.LEGACY_ALGO_LIST);
				}
				if (useKnownHosts) {
					config.put(SFTPConstants.PROP_STRICT_HOST_KEY_CHECKING, SFTPConstants.YES);
				}
				switch (authType) {
				case PUBLIC_KEY:
					if (!publicKeyparam.isUseKeyContentEnabled()) {
						jsch.addIdentity(publicKeyparam.getPrvkeyPath(), publicKeyparam.getPassphrase());
					} else {
						jsch.addIdentity(publicKeyparam.getKeyPairName(), publicKeyparam.getPrvkeyContent(),
								publicKeyparam.getPubkeyContent(), publicKeyparam.getPassphraseContent());

					}
					session = jsch.getSession(publicKeyparam.getUser(), host, port);
					break;
				case PASSWORD:
					session = jsch.getSession(passwordParam.getUser(), host, port);
					session.setPassword(passwordParam.getPassword());
					break;
				}

				session.setConfig(config);
				session.setDaemonThread(true);
				session.connect(60000);
			}

		} catch (UnsupportedEncodingException e) {
			close();
			throw new ConnectorException(SFTPConstants.UNABLE_TO_PPROCESS_KNOWN_HOST_KEY + e.getMessage(),
					(Throwable) e);
		} catch (JSchException e) {
			if (session == null) {
				throw new ConnectorException(
						SFTPConstants.ERROR_FAILED_CREATING_SESSION + SFTPConstants.CAUSE + e.getMessage(), e);
			}
			if (!session.isConnected()) {
				close();
				if (e.getCause() instanceof java.net.UnknownHostException) {
					throw new ConnectorException(
							SFTPConstants.ERROR_FAILED_CONNECTION_TO_HOST + SFTPConstants.CAUSE + e.getMessage(), e);
				}
				if (e.getCause() instanceof java.net.ConnectException) {
					throw new ConnectorException(
							SFTPConstants.ERROR_FAILED_CONNECTION_TO_PORT + SFTPConstants.CAUSE + e.getMessage());
				} else {
					throw new ConnectorException(
							SFTPConstants.ERROR_FAILED_SFTP_LOGIN + SFTPConstants.CAUSE + e.getMessage());
				}
			} else {
				close();
				throw new ConnectorException(SFTPConstants.ERROR_FAILED_SFTP_SERVER_CONNECTION
						+ SFTPConstants.ERROR_FAILED_SFTP_LOGIN + SFTPConstants.CAUSE + e.getMessage(), e);
			}
		} catch (Exception e) {
			close();
			throw new ConnectorException(SFTPConstants.ERROR_FAILED_SFTP_SERVER_CONNECTION
					+ SFTPConstants.ERROR_FAILED_SFTP_LOGIN + SFTPConstants.CAUSE + e.getMessage(), e);
		} finally {
			if (knownHosts != null) {
				IOUtil.closeQuietly(knownHosts);
			}
			if (this.session == null || !session.isConnected()) {
				close();
			}
		}

	}

	public void closeConnection() {
		killChannel();
		killSession();
	}

	private void killChannel() {
		if (this.sftpChannel != null && this.sftpChannel.isConnected()) {
			try {
				this.logger.fine(SFTPConstants.DISCONNECTING_FROM_SFTP_SERVER);
				this.sftpChannel.disconnect();
			} catch (Exception e) {
				this.logger.log(Level.FINE, SFTPConstants.ERROR_DISCONNECTING_CHANNEL, e);
			}
		}
	}

	private void killSession() {
		if (this.session != null && this.session.isConnected()) {
			try {
				this.session.disconnect();
			} catch (Exception e) {
				this.logger.log(Level.FINE, SFTPConstants.ERROR_DISCONNECTING_SESSION, e);
			}
		}
	}

	public void getChannel() {
		try {
			Channel channel = session.openChannel(SFTPConstants.SFTP);
			channel.connect();
			this.sftpChannel = (ChannelSftp) channel;
		} catch (JSchException e) {
			closeConnection();
			throw new ConnectorException(SFTPConstants.ERROR_FAILED_OPENING_SFTP_CHANNEL, e);
		}
	}

	public void putFile(InputStream content, String filePath, int mode) {

		try {
			sftpChannel.put(content, filePath, mode);
		} catch (SftpException e) {
			throw new SFTPSdkException(MessageFormat.format(SFTPConstants.ERROR_FAILED_FILE_UPLOAD, filePath)
					+ SFTPConstants.CAUSE + e.getMessage(), e);
		}

	}

	public void renameFile(String filePath, String newFilePath) {

		try {
			sftpChannel.rename(filePath, newFilePath);
		} catch (SftpException e) {
			throw new SFTPSdkException(MessageFormat.format(SFTPConstants.ERROR_FAILED_RENAME, filePath, newFilePath)
					+ SFTPConstants.CAUSE + e.getMessage(), e);
		}
	}

	public boolean isFilePresent(String filePath) {

		try {
			sftpChannel.stat(filePath);
			return true;
		} catch (SftpException e) {
			if (e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE)
				return false;
			else {
				throw new SFTPSdkException(
						MessageFormat.format(SFTPConstants.ERROR_FAILED_CHECKING_FILE_EXISTENCE, filePath)
								+ SFTPConstants.CAUSE + e.getMessage(),
						e);
			}
		}
	}

	public void deleteFile(String filePath) {

		try {
			sftpChannel.rm(filePath);
		} catch (SftpException e) {
			if (e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE) {
				throw new NoSuchFileFoundException(SFTPConstants.FILE_NOT_FOUND, e);
			}
			throw new SFTPSdkException(MessageFormat.format(SFTPConstants.ERROR_FAILED_FILE_REMOVAL, filePath)
					+ SFTPConstants.CAUSE + e.getMessage(), e);
		}

	}

	public InputStream getFileStream(String filePath) {

		try {
			return sftpChannel.get(filePath);
		} catch (SftpException e) {
			if (e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE) {
				throw new NoSuchFileFoundException(SFTPConstants.FILE_NOT_FOUND, e);
			}
			throw new SFTPSdkException(MessageFormat.format(SFTPConstants.ERROR_FAILED_FILE_RETRIEVAL, filePath)
					+ SFTPConstants.CAUSE + e.getMessage(), e);
		}

	}

	static boolean canRetrieve(ChannelSftp.LsEntry entry) {
		return !entry.getAttrs().isDir() && !entry.getAttrs().isLink()
				&& !StringUtil.isBlank((String) entry.getFilename());
	}

	public String getUniqueFileName(String path, String inputfileName) {
		final String inputFileName = inputfileName;
		final int inpFileNameLength = inputFileName.indexOf('.');
		retFileName = inputfileName;
		LsEntrySelector selector = new LsEntrySelector() {
			public int select(LsEntry entry) {
				final String filename = entry.getFilename();
				if (filename.equals(".") || filename.equals("..")) {
					return CONTINUE;
				}
				if (entry.getAttrs().isLink() || entry.getAttrs().isDir()) {
					return CONTINUE;
				}
				if (inputFileName.equals(filename)) {
					String ienteredFileName = inputFileName.substring(0, inpFileNameLength);
					String ext = inputFileName.substring(inputFileName.lastIndexOf('.'), inputFileName.length());
					retFileName = ienteredFileName.concat("1").concat(ext); 
					filePresence=true;
				} 
				else {
						if (filename.length() > inputFileName.length() && filePresence && retFileName.equals(filename)) {
							String ifilename = filename.substring(0, inpFileNameLength);
							String ienteredFileName = inputFileName.substring(0, inpFileNameLength);
							String subString = filename.substring(inpFileNameLength, filename.lastIndexOf('.'));
							String ext = filename.substring(filename.lastIndexOf('.'), filename.length());
							if (ifilename.equals(ienteredFileName) && StringUtils.isNumeric(subString)) {
	                            int append = Integer.parseInt(subString);
	                            retFileName = ienteredFileName.concat(String.valueOf(append+1)).concat(ext);
							}
						}
					}
				return CONTINUE;
			}
		};
		try {
			sftpChannel.ls(path, selector);
		} catch (SftpException e) {
			throw new SFTPSdkException(
					SFTPConstants.ERROR_FAILED_LISTING_FILENAMES + SFTPConstants.CAUSE + e.getMessage(), e);
		}
		return retFileName;
	}

	public void createDirectory(String filePath) {
		try {
			sftpChannel.mkdir(filePath);
		} catch (SftpException e) {
			throw new SFTPSdkException(MessageFormat.format(SFTPConstants.ERROR_FAILED_DIRECTORY_CREATE, filePath)
					+ SFTPConstants.CAUSE + e.getMessage(), e);
		}
	}

	public String getCurrentDirectory() {
		try {
			return sftpChannel.pwd();
		} catch (SftpException e) {

			throw new SFTPSdkException(
					SFTPConstants.ERROR_FAILED_RETRIEVING_DIRECTORY + SFTPConstants.CAUSE + e.getMessage(), e);
		}

	}

	public boolean isConnected() {

		return sftpChannel != null && sftpChannel.isConnected() && !sftpChannel.isClosed() && session != null
				&& session.isConnected();

	}

	public void changeCurrentDirectory(String fullPath) {
		try {
			sftpChannel.cd(fullPath);
		} catch (SftpException e) {
			throw new SFTPSdkException(
					MessageFormat.format(SFTPConstants.ERROR_FAILED_CHANGING_CURRENT_DIRECTORY, fullPath)
							+ SFTPConstants.CAUSE + e.getMessage(),
					e);
		}
	}

	public SftpATTRS getFileMetadata(String fileFullPath) {

		try {
			return sftpChannel.lstat(fileFullPath);
		} catch (SftpException e) {
			throw new SFTPSdkException(
					MessageFormat.format(SFTPConstants.ERROR_FAILED_GETING_FILE_METADATA, fileFullPath)
							+ SFTPConstants.CAUSE + e.getMessage(),
					e);

		}
	}

	public void createNestedDirectory(String fullPath) {
		String[] folders = fullPath.split("/");
		changeCurrentDirectory("/");
		for (String folder : folders) {
			if (folder.length() > 0) {
				try {
					changeCurrentDirectory(folder);
				} catch (SFTPSdkException e) {
					createDirectory(folder);
					changeCurrentDirectory(folder);
				}
			}
		}
	}

	public String getHomeDirectory() {
		try {
			return sftpChannel.getHome();
		} catch (SftpException e) {
			throw new SFTPSdkException(SFTPConstants.ERROR_GETTING_HOME_DIR + SFTPConstants.CAUSE + e.getMessage());
		}
	}

	public void retrieveFile(String fileName, MeteredTempOutputStream outputStream, long noOfBytestoSkip) {

		try {
			sftpChannel.get(fileName, outputStream, null, ChannelSftp.RESUME, noOfBytestoSkip);
		} catch (SftpException e) {
			if (e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE) {
				throw new NoSuchFileFoundException(SFTPConstants.FILE_NOT_FOUND, e);
			}
			throw new SFTPSdkException(MessageFormat.format(SFTPConstants.ERROR_FAILED_FILE_RETRIEVAL, fileName)
					+ SFTPConstants.CAUSE + e.getMessage(), e);
		}

	}

	public void listDirectoryContentWithSelector(String dirfullPath, LsEntrySelector selector) {

		try {
			sftpChannel.ls(dirfullPath, selector);
		} catch (SftpException e) {
			throw new SFTPSdkException(
					MessageFormat.format(SFTPConstants.ERROR_FAILED_GETING_DIRECTORY_CONTENTS, dirfullPath)
							+ SFTPConstants.CAUSE + e.getMessage(),
					e);
		}
	}

	@Override
	public void close() {
		closeConnection();

	}
}
