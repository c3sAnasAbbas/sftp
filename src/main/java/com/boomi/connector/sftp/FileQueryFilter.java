//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp;

import com.boomi.connector.api.Expression;
import com.boomi.connector.api.FilterData;
import com.boomi.connector.api.GroupingExpression;
import com.boomi.connector.api.GroupingOperator;
import com.boomi.connector.api.SimpleExpression;
import com.boomi.connector.sftp.common.FileMetadata;
import com.boomi.connector.sftp.common.UnixPathsHandler;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.util.CollectionUtil;
import com.boomi.util.LogUtil;
import com.jcraft.jsch.ChannelSftp.LsEntry;

import java.io.File;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
public class FileQueryFilter implements DirectoryStream.Filter<LsEntry> {

	private static final CollectionUtil.Function<LsEntry, String> GET_FILENAME = new CollectionUtil.Function<LsEntry, String>() {

		public String apply(LsEntry fileMetadata) {

			return fileMetadata.getFilename();
		}
	};
	private static final CollectionUtil.Function<LsEntry, Long> GET_FILESIZE = new CollectionUtil.Function<LsEntry, Long>() {

		public Long apply(LsEntry fileMetadata) {
			return fileMetadata.getAttrs().getSize();
		}
	};
	private static final CollectionUtil.Function<LsEntry, Boolean> IS_DIRECTORY = new CollectionUtil.Function<LsEntry, Boolean>() {

		public Boolean apply(LsEntry fileMetadata) {
			return fileMetadata.getAttrs().isDir();
		}
	};

	private static final CollectionUtil.Function<LsEntry, Date> GET_MODIFIED_DATE = new CollectionUtil.Function<LsEntry, Date>() {

		public Date apply(LsEntry fileMetadata) {
			try {
				return FileMetadata.parseDate(
						FileMetadata.formatDate(FileMetadata.parseDate(fileMetadata.getAttrs().getMTime() * 1000l)));
			} catch (ParseException e) {
				throw new IllegalArgumentException(SFTPConstants.ERROR_PARSING_DATE_FROM_FILESYSTEM, e);
			}
		}
	};
	private static final CollectionUtil.Filter<LsEntry> DEFAULT_FILTER = CollectionUtil.acceptAllFilter();
	private final CollectionUtil.Filter<LsEntry> _subFilter;
	private final Logger _logger;
	private final Path _directory;
	private FilterData input;
	private String dirFullPath;

	public FilterData getInput() {
		return input;
	}

	public FileQueryFilter(File directory, FilterData input, String dirFullPath) {

		this._directory = directory.toPath();
		this.input = input;
		this._logger = input.getLogger();
		Expression rootExpr = input.getFilter().getExpression();
		this._subFilter = rootExpr == null ? DEFAULT_FILTER : this.buildFilter(rootExpr);
		this.dirFullPath = dirFullPath;

	}

	@Override
	public boolean accept(LsEntry meta) {
		return this._subFilter.accept(meta);
	}

	private CollectionUtil.Filter<LsEntry> buildFilter(Expression expression) {
		if (expression instanceof GroupingExpression) {
			return this.group((GroupingExpression) expression);
		}
		if (expression instanceof SimpleExpression) {
			return this.simple((SimpleExpression) expression);
		}
		throw new IllegalArgumentException(SFTPConstants.UNKNOWN_EXPRESSION + expression.getClass().getCanonicalName());
	}

	private CollectionUtil.Filter<LsEntry> group(GroupingExpression expression) {
		LinkedList<CollectionUtil.Filter<LsEntry>> filters = new LinkedList<>();
		for (Expression e : expression.getNestedExpressions()) {
			filters.add(this.buildFilter(e));
		}
		return new FilterGroup(filters, expression.getOperator());
	}

	private CollectionUtil.Filter<LsEntry> simple(SimpleExpression expression) {
		String opStr = expression.getOperator();
		String prop = expression.getProperty();
		int argCount = expression.getArguments().size();
		if (argCount != 1) {
			throw new IllegalArgumentException(
					MessageFormat.format(SFTPConstants.EXACTLY_ONE_ARGUEMENT_REQUIRED, argCount));

		}
		String arg = expression.getArguments().get(0);
		if (SFTPConstants.OP_REGEX.equals(opStr) || SFTPConstants.OP_WILDCARD.equals(opStr)) {
			if (!SFTPConstants.PROPERTY_FILENAME.equals(prop)) {
				throw new UnsupportedOperationException(SFTPConstants.PATTERN_UNSUPPORTED_FOR_FILENAMES);
			}
			return new PatternFilter(opStr, arg);
		}
		Operation op = Operation.valueOf(opStr);
		CompFilter compFilter = null;
		switch (prop) {
		case SFTPConstants.PROPERTY_FILENAME: {
			compFilter = new CompFilter(GET_FILENAME, op, (Comparable) ((Object) arg));
			break;
		}
		case SFTPConstants.FILESIZE: {
			compFilter = new CompFilter(GET_FILESIZE, op, Long.valueOf(Long.parseLong(arg)));
			break;
		}
		case SFTPConstants.IS_DIRECTORY: {
			compFilter = new CompFilter(IS_DIRECTORY, op, Boolean.valueOf(FileQueryFilter.parseBoolean(arg)));
			break;
		}

		case SFTPConstants.MODIFIED_DATE: {
			compFilter = new CompFilter(GET_MODIFIED_DATE, op, FileQueryFilter.parseDate(arg));
			break;
		}
		default: {
			throw new IllegalArgumentException(SFTPConstants.UNKNOWN_PROPERTY + prop);
		}
		}
		return compFilter;
	}

	private static boolean parseBoolean(String input) {
		if ("true".equalsIgnoreCase(input)) {
			return true;
		}
		if ("false".equalsIgnoreCase(input)) {
			return false;
		}
		throw new IllegalArgumentException(SFTPConstants.QUOTE + input + SFTPConstants.INVALID_BOOLEAN_VALUE);
	}

	private static Date parseDate(String date) {
		try {
			return FileMetadata.parseDate(date);
		} catch (ParseException e) {
			throw new IllegalArgumentException(SFTPConstants.UNABLE_TO_PARSE_DATE + date + SFTPConstants.QUOTE, e);
		}
	}

	private class PatternFilter implements CollectionUtil.Filter<LsEntry> {
		private final String _op;
		private final PathMatcher _matcher;
		private final String _pattern;

		PatternFilter(FileSystem fs, String op, String pattern) {
			String mode = SFTPConstants.OP_REGEX.equals(op) ? SFTPConstants.REGEX : SFTPConstants.GLOB;
			this._matcher = fs.getPathMatcher(mode + ':' + pattern);
			this._pattern = pattern;
			this._op = op;
		}

		PatternFilter(String op, String pattern) {
			this(FileSystems.getDefault(), op, pattern);
		}

		public boolean accept(LsEntry meta) {
			String fullFilePath = new UnixPathsHandler().joinPaths(dirFullPath, meta.getFilename());
			File fullFilePathObj = new File(fullFilePath);
			Path path = FileQueryFilter.this._directory.relativize(fullFilePathObj.toPath());
			boolean res = this._matcher.matches(path);
			if (FileQueryFilter.this._logger.isLoggable(Level.FINE)) {
				LogUtil.fine((Logger) FileQueryFilter.this._logger, SFTPConstants.MATCHING_WITH_PATTERN,
						(Object[]) new Object[] { path.toString(), this._pattern, this._op, res });
			}
			return res;
		}
	}

	private class CompFilter<E extends Comparable<E>> implements CollectionUtil.Filter<LsEntry> {
		private final CollectionUtil.Function<LsEntry, E> _leftGetter;
		private final Operation _op;
		private final E _rightSide;

		private CompFilter(CollectionUtil.Function<LsEntry, E> leftGetter, Operation op, E rightSide) {
			this._leftGetter = leftGetter;
			this._op = op;
			this._rightSide = rightSide;
		}

		public boolean accept(LsEntry fileMetadata) {
			E left = this._leftGetter.apply(fileMetadata);
			boolean res = this._op.accept(left, this._rightSide);
			if (_logger.isLoggable(Level.FINE)) {
				LogUtil.fine((Logger) _logger, (String) SFTPConstants.COMPARISON_YIELDS,
						(Object[]) new Object[] { left, this._op, this._rightSide, res });
			}
			return res;
		}
	}

	private static class FilterGroup implements CollectionUtil.Filter<LsEntry> {
		private final List<CollectionUtil.Filter<LsEntry>> _filters;
		private final boolean _initialVal;

		protected FilterGroup(List<CollectionUtil.Filter<LsEntry>> filters, GroupingOperator op) {
			this._filters = filters;
			if (op == GroupingOperator.AND) {
				this._initialVal = true;
			} else if (op == GroupingOperator.OR) {
				this._initialVal = false;
			} else {
				throw new IllegalArgumentException(SFTPConstants.UNKNOWN_GROUPING_OPERATOR + op);
			}
		}

		public boolean accept(LsEntry fileMetadata) {
			boolean res;
			res = this._initialVal;
			Iterator<CollectionUtil.Filter<LsEntry>> i$ = this._filters.iterator();
			while (i$.hasNext() && (res = i$.next().accept(fileMetadata)) == this._initialVal) {
			}
			return res;
		}
	}

	private static enum Operation {
		EQUALS(new CollectionUtil.Filter<Integer>() {

			public boolean accept(Integer integer) {
				return integer == 0;
			}
		}), NOT_EQUALS(new CollectionUtil.Filter<Integer>() {

			public boolean accept(Integer integer) {
				return integer != 0;
			}
		}), LESS_THAN(new CollectionUtil.Filter<Integer>() {

			public boolean accept(Integer integer) {
				return integer < 0;
			}
		}), GREATER_THAN(new CollectionUtil.Filter<Integer>() {

			public boolean accept(Integer integer) {
				return integer > 0;
			}
		});

		private final CollectionUtil.Filter<Integer> _filter;

		private Operation(CollectionUtil.Filter<Integer> filter) {
			this._filter = filter;
		}

		public <E extends Comparable<E>> boolean accept(E left, E right) {
			return this._filter.accept(left.compareTo(right));
		}

	}

}
